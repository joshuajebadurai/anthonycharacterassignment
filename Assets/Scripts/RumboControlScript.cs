using UnityEngine;
using System.Collections;

// Require these components when using this script
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Rigidbody))]
public class RumboControlScript : MonoBehaviour
{	
		public float animSpeed = 1.5f;					// a public setting for overall animator animation speed
		public float lookSmoother = 3f;					// a smoothing setting for camera motion
		public Camera mainCamera;						// a reference to the Main Camera
		public Camera staticCamera;						// a reference to the static Camera
		private Animator anim;							// a reference to the animator on the character
		private AnimatorStateInfo currentBaseState;		// a reference to the current state of the animator, used for base layer
		private AnimatorStateInfo layer2CurrentState;	// a reference to the current state of the animator, used for layer 2

		static int idleState = Animator.StringToHash ("Base Layer.Idle");
		static int locoState = Animator.StringToHash ("Base Layer.Locomotion");			// these integers are references to our animator's states
		static int jumpState = Animator.StringToHash ("Base Layer.Jump");				// and are used to check state for various actions to occur
		static int waveState = Animator.StringToHash ("Layer2.Wave");					// within our FixedUpdate() function below

		void Start ()
		{
				// initialising reference variables
				anim = GetComponent<Animator> ();					  
				if (anim.layerCount == 2)
						anim.SetLayerWeight (1, 1);
				mainCamera.enabled = true;
				staticCamera.enabled = false;
		}
	
		void FixedUpdate ()
		{
				if (Input.GetKeyDown (KeyCode.C)) {
						mainCamera.enabled = !mainCamera.enabled;
						staticCamera.enabled = !staticCamera.enabled;
				}

				float h = Input.GetAxis ("Horizontal");						// setup h variable as our horizontal input axis
				float v = Input.GetAxis ("Vertical");						// setup v variables as our vertical input axis
				anim.SetFloat ("Speed", v);									// set our animator's float parameter 'Speed' equal to the vertical input axis				
				anim.SetFloat ("Direction", h); 							// set our animator's float parameter 'Direction' equal to the horizontal input axis		
				anim.speed = animSpeed;										// set the speed of our animator to the public variable 'animSpeed'
				currentBaseState = anim.GetCurrentAnimatorStateInfo (0);	// set our currentState variable to the current state of the Base Layer (0) of animation
		
				if (anim.layerCount == 2)		
						layer2CurrentState = anim.GetCurrentAnimatorStateInfo (1);	// set our layer2CurrentState variable to the current state of the second Layer (1) of animation
		
		if (Input.GetKey (KeyCode.LeftShift) && Input.GetKey (KeyCode.W)) {
						anim.SetFloat ("Speed", 0.2f); 									//set our animator's float parameter 'Speed' equal to a lesser value to make the character walk
				}
				// STANDARD JUMPING
		
				// if we are currently in a state called Locomotion , then allow Jump input (Space) to set the Jump bool parameter in the Animator to true
				if (currentBaseState.nameHash == locoState) {
						if (Input.GetButtonDown ("Jump")) {
								anim.SetBool ("Jump", true);
						}
				}
		
		// if we are in the jumping state... 
		else if (currentBaseState.nameHash == jumpState) {
						//  ..and not still in transition..
						if (!anim.IsInTransition (0)) {				
								// reset the Jump bool so we can jump again, and so that the state does not loop 
								anim.SetBool ("Jump", false);
						}
			
						// Raycast down from the center of the character.. 
						Ray ray = new Ray (transform.position + Vector3.up, -Vector3.up);
						RaycastHit hitInfo = new RaycastHit ();
			
						if (Physics.Raycast (ray, out hitInfo)) {
								// ..if distance to the ground is more than 1.75, use Match Target
								if (hitInfo.distance > 1.75f) {
					
										// MatchTarget allows us to take over animation and smoothly transition our character towards a location - the hit point from the ray.
										// Here we're telling the Root of the character to only be influenced on the Y axis (MatchTargetWeightMask) and only occur between 0.35 and 0.5
										// of the timeline of our animation clip
										anim.MatchTarget (hitInfo.point, Quaternion.identity, AvatarTarget.Root, new MatchTargetWeightMask (new Vector3 (0, 1, 0), 0), 0.35f, 0.5f);
								}
						}
				}

				// IDLE
				
				// check if we are at idle, if so, let us Wave!
				else if (currentBaseState.nameHash == idleState) {
								if (Input.GetButtonUp ("Jump")) {
										anim.SetBool ("Wave", true);
								}
						}
				// if we enter the waving state, reset the bool to let us wave again in future
				if (layer2CurrentState.nameHash == waveState) {
						anim.SetBool ("Wave", false);
				}
		}
}
